# Thema Matrizenrechnung

## Team Analita Adriana Raiwaki ; Christelle Laure Tchedi; Valentin Yefouo

## Funktionsbeschreibung
- Matrix erstellen
- Matrix multiplikation
- Determinante von Matrizen
- Addition von Matrizen
- Matrizen mit einer Zahl
- Matrix mit einer Vektor
- Transponierte Matrix

## Bezug zu Mathematik
   Linear Algebra

## Zeitplan
- Grobkonzept Ende Januar
- Feinkonzept Ende Januar
- Grundgerüst der Programmierung Ende Februar
- Prototyp für den Test Ende März

## Aufgabenteilung
- gemeinsame Erarbeitung

## Zusammenarbeit
- Regelmäßige Treffen in der Hochschule

## Dokumentation
- Basierend auf Javadoc
- Sprache Deutsch
- Basierend auf Word/ PowerPoint


## Offene Punkte, Fragen


